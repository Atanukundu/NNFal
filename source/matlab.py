import subprocess



def validate(inputs, matlab_config):
	print(inputs)
	inputs_str = ' '.join(str(i) for i in inputs)
	print(inputs_str)
	matlab_cmd = "python3 " + matlab_config + " " + inputs_str
	print(matlab_cmd)
	output_matlab = str(subprocess.run(matlab_cmd, capture_output=True, shell=True))
	print(output_matlab)
	print(output_matlab.find("Validated"))
	if(output_matlab.find("Validated") != -1):
		return True
	else:
		return False
