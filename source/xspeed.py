import os
import subprocess
import re

#XSpeed-plan must be installed in this directory.

def parse_variable(config_file):
	file1 = open(config_file)
	lines = [line.rstrip() for line in file1]
	file1.close()
	#print(lines)
	variables = []
	time_step = []
	for line in lines:
		if 'variable' in line:
			variables = line.split('=')
		if 'time-step' in line:
			time_step = line.split('=')
			#print(variables[1])
	return variables[1], time_step[1]
	
def simulate(inputs,model_file,config_file):
	print(inputs)
	working_dir = os.getcwd()
	sys_variable, time_step = parse_variable(config_file)
	sys_variable = sys_variable.split(',')
	#print(sys_variable)
		
	os.chdir("..")
	os.chdir(str(os.getcwd()) + "/validation/XSpeed-plan/build")
	xspeed_cmd = "./XSpeed-plan -m ../../../source/" + model_file + " -c ../../../source/" + config_file + " --time-horizon 100 --time-step " + time_step.strip() + " --depth 100 -v " + sys_variable[0].strip() + "," + sys_variable[1].strip() + " -e validation"
	#print(xspeed_cmd)
	
	x_cmd = ' --initial "' 
	for index,var in enumerate(sys_variable):
		if(index == len(sys_variable)-1):
			x_cmd += "1*" + str(var.strip()) + " == " + str(inputs[index])
		else:
			x_cmd += "1*" + str(var.strip()) + " == " + str(inputs[index]) + " & "
	x_cmd += '"'
	xspeed_cmd += x_cmd
	#print(xspeed_cmd)
	output1 = str(subprocess.run(xspeed_cmd, capture_output=True, shell=True))
	os.chdir(working_dir)
	#print(output1)
	violated = "Is violated trajectory found:"
	vio_traj = output1.find(violated)
	vio_traj_start = int(vio_traj)+len(violated)+0
	vio_traj_end = int(vio_traj)+len(violated)+16
	vio_traj = output1[vio_traj_start:vio_traj_end];
	vio_traj = re.sub("[^\d\.]", "", vio_traj)

	if (int(vio_traj) > 0):
		return True
	else:
		return False
