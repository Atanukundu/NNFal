import sys
import argparse
import subprocess
import re
import numpy as np
import os
#import setting
import xspeed
import scale
import dnnf
import time
import matlab





if __name__ == "__main__":
	
	print("                                            ▄▄  \n▀███▄   ▀███▀███▄   ▀███▀███▀▀▀███        ▀███  \n  ███▄    █   ███▄    █   ██    ▀█          ██  \n  █ ███   █   █ ███   █   ██   █  ▄█▀██▄    ██  \n  █  ▀██▄ █   █  ▀██▄ █   ██▀▀██ ██   ██    ██  \n  █   ▀██▄█   █   ▀██▄█   ██   █  ▄█████    ██  \n  █     ███   █     ███   ██     ██   ██    ██  \n ▄███▄    ██ ▄███▄    ██ ▄████▄   ▀████▀██▄▄████▄")

		
	parser = argparse.ArgumentParser()# Add an argument
	parser.add_argument('--property', type=str, required=True, help = "Safety property in DNNP.")
	parser.add_argument('--network', type=str, required=True, help = "Neural network in ONNX.")
	parser.add_argument('--CPS_Rep', type=str, required=True, help = "CPS type i.e HA or MATLAB")
	parser.add_argument('--model', type=str, help = "Model file (XML) of the HA model required for validation.")
	parser.add_argument('--config', type=str, help = "Config file (cfg) of the HA model required for validation.")
	parser.add_argument('--initial', type=str, help = "Initial config file (py) for the matlab model required for validation.")
	parser.add_argument('--scaling', type=str, required=True, help = "Mention the dataset/model for inv_scalling(CE)")
	parser.add_argument('--falsifier', type=str, required=True, help = "Falsifier should be in lower case latter: pgd, nnenum")
	#parser.add_argument('--timeout', type=str, required=True, help = "Set timeout for the running time.")
	args = parser.parse_args()
	#print(args.property)
	#sys_variable = xspeed.parse_variable(args.config).split(',')
	#print(args.initial)
	cmd = ''
	if(args.falsifier == "pgd"):
		cmd = "dnnf " + args.property + " --network N " + args.network + " --save-violation violation --backend " + args.falsifier
		
	if(args.falsifier == "nnenum"):
		cmd = "dnnv " + args.property + " --network N " + args.network + " --save-violation violation --" + args.falsifier
	#print(cmd)
	f_time = 0
	r_time = 0
	v_time = 0
	refine_counter= 0
	while(True):
		output = str(subprocess.run(cmd, capture_output=True, shell=True))
		#print(output)
		status = output.find("result: sat")
		status_memory = output.find("out of memory")
		status_time = output.find("out of time")
		if(status == -1):
			if(refine_counter != 0):
				dnnf.setInitialProperty(args.property)
			if(status_memory != -1):
				print("DNNF can't find CE due to OOM.")
			if(status_time != -1):
				print("DNNF can't find CE due to Timeout.")
			break;
		else:
			print("Found a CE input.")
			res = " result:"
			res_str = output.find(res)
			res_start = int(res_str)+len(res)+0
			res_end = int(res_str)+len(res)+4
			result = output[res_start:res_end];
			if(args.falsifier == "pgd"):
				res2 = "falsification time:"
				res2_str = output.find(res2)
				res2_start = int(res2_str)+len(res2)+0
				res2_end = int(res2_str)+len(res2)+16
				result2 = output[res2_start:res2_end];
				d_time = re.sub("[^\d\.]", "", result2)
			if(args.falsifier == "nnenum"):
				res2 = "time:"
				res2_str = output.find(res2)
				res2_start = int(res2_str)+len(res2)+0
				res2_end = int(res2_str)+len(res2)+16
				result2 = output[res2_start:res2_end];
				d_time = re.sub("[^\d\.]", "", result2)

			#dnnf returns the Input and stored it into the list inputs as per the dimension.
			Input = np.load("violation.npy")
			print(Input.tolist()[0])
			inputs = Input.tolist()[0]
			os.remove("violation.npy")
			val_time = time.time()
			#Currently NNFal support only two CPS models: "MATLAB" and "HA"
			if(args.CPS_Rep == "MATLAB"):
				#isReach, csvInputs =setting.common(inputs,args.scaling)
				isReach = matlab.validate(scale.inv_scaling(Input.tolist()[0],args.scaling), args.initial)
			if(args.CPS_Rep == "HA"):
				isReach = xspeed.simulate(scale.inv_scaling(Input.tolist()[0], args.scaling),args.model,args.config)
			#print(isReach)
			val_time = time.time() - val_time
			#print("Validation time: ", val_time)
			if (isReach):
				print("Found a trajectory!!!")
				print("Falsifer time: ",f_time + float(d_time))
				print("Refinement needed: ",refine_counter)
				print("Validation time: ", v_time + val_time)
				print("Total time required: ",(v_time + val_time)+float(d_time) + f_time)
				if(refine_counter != 0):
					dnnf.setInitialProperty(args.property)
				break;
			else:
				print("Can't find a trajectory, refined the property and searching for a CE...")
				refine_counter = refine_counter + 1
				ref_time = time.time()
				dnnf.refined_property(Input.tolist()[0],args.property,refine_counter)
				#print("Refine time: ",time.time() - ref_time)
				f_time +=float(d_time)
				v_time += float(val_time) + (time.time() - ref_time)

