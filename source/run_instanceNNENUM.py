import os
import subprocess
import re
import numpy as np
import xspeed
import dnnf
import time
import scale
import csv


instance = []

# HA instances:
instance.append(['Two_tanks P1','runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/Two_tanks/property_11.py --network ../network/Two_tanks/Two_tank_NN-1.onnx --falsifier nnenum --scaling Two_tanks_NN --CPS_Rep HA --model ../validation/XSpeed-plan/benchmarks/NNFal/two_tanks.xml --config ../validation/XSpeed-plan/benchmarks/NNFal/two_tanks_1.cfg'])
instance.append(['Two_tanks P2', 'runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/Two_tanks/property_22.py --network ../network/Two_tanks/Two_tank_NN-1.onnx --falsifier nnenum --scaling Two_tanks_NN --CPS_Rep HA --model ../validation/XSpeed-plan/benchmarks/NNFal/two_tanks.xml --config ../validation/XSpeed-plan/benchmarks/NNFal/two_tanks_3.cfg'])
instance.append(['Two_tanks P3', 'runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/Two_tanks/property_33.py --network ../network/Two_tanks/Two_tank_NN-1.onnx --falsifier nnenum --scaling Two_tanks_NN --CPS_Rep HA --model ../validation/XSpeed-plan/benchmarks/NNFal/two_tanks.xml --config ../validation/XSpeed-plan/benchmarks/NNFal/two_tanks_4.cfg'])
instance.append(['Two_tanks P4', 'runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/Two_tanks/property_44.py --network ../network/Two_tanks/Two_tank_NN-1.onnx --falsifier nnenum --scaling Two_tanks_NN --CPS_Rep HA --model ../validation/XSpeed-plan/benchmarks/NNFal/two_tanks.xml --config ../validation/XSpeed-plan/benchmarks/NNFal/two_tanks_5.cfg'])

instance.append(['Oscillator P1', 'runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/Oscillator/property_11.py --network ../network/Oscillator/osc_ES_5.onnx --falsifier nnenum --scaling Oscillator_NN --CPS_Rep HA --model ../validation/XSpeed-plan/benchmarks/NNFal/Oscillator.xml --config ../validation/XSpeed-plan/benchmarks/NNFal/Oscillator_1.cfg'])
#instance.append(['Oscillator P2', 'runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/Oscillator/property_22.py --network ../network/Oscillator/osc_ES_5.onnx --falsifier nnenum --scaling Oscillator_NN --CPS_Rep HA --model ../validation/XSpeed-plan/benchmarks/NNFal/Oscillator.xml --config ../validation/XSpeed-plan/benchmarks/NNFal/Oscillator_2.cfg'])
instance.append(['Oscillator P3', 'runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/Oscillator/property_33.py --network ../network/Oscillator/osc_ES_5.onnx --falsifier nnenum --scaling Oscillator_NN --CPS_Rep HA --model ../validation/XSpeed-plan/benchmarks/NNFal/Oscillator.xml --config ../validation/XSpeed-plan/benchmarks/NNFal/Oscillator_3.cfg'])

instance.append(['NAV_30 P1', 'runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/NAV/NAV_30/property_11.py --network ../network/NAV/NAV_30/NAV_30_NN-1.onnx --falsifier nnenum --scaling NAV_30_NN --CPS_Rep HA --model ../validation/XSpeed-plan/benchmarks/NNFal/30.xml --config ../validation/XSpeed-plan/benchmarks/NNFal/30_1.cfg'])
instance.append(['NAV_30 P2', 'runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/NAV/NAV_30/property_22.py --network ../network/NAV/NAV_30/NAV_30_NN-1.onnx --falsifier nnenum --scaling NAV_30_NN --CPS_Rep HA --model ../validation/XSpeed-plan/benchmarks/NNFal/30.xml --config ../validation/XSpeed-plan/benchmarks/NNFal/30_2.cfg'])
instance.append(['NAV_30 P3', 'runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/NAV/NAV_30/property_33.py --network ../network/NAV/NAV_30/NAV_30_NN-1.onnx --falsifier nnenum --scaling NAV_30_NN --CPS_Rep HA --model ../validation/XSpeed-plan/benchmarks/NNFal/30.xml --config ../validation/XSpeed-plan/benchmarks/NNFal/30_3.cfg'])

#instance.append(['Bball P1','runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/Bball/property_11.py --network ../network/Bball/BBALL_ES_5.onnx --falsifier nnenum --scaling B_Ball_NN --CPS_Rep HA --model ../validation/XSpeed-plan/benchmarks/NNFal/bball.xml --config ../validation/XSpeed-plan/benchmarks/NNFal/bball.cfg'])

instance.append(['ACC P1','runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/ACCU/property_11.py --network ../network/ACCU/ACCU04_ES_5.onnx --falsifier nnenum --scaling ACCU04_NN --CPS_Rep HA --model ../validation/XSpeed-plan/benchmarks/NNFal/ACCU04.xml --config ../validation/XSpeed-plan/benchmarks/NNFal/ACCU04.cfg'])


# Matlab instances:
instance.append(['F16 P1', 'runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/F16/property_11.py --network ../network/F16/F16_ES_5.onnx --falsifier nnenum --scaling F16 --CPS_Rep MATLAB --initial ../validation/matlab/F_16/initial_F16.py'])

instance.append(['AT P1','runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/AT/property_11.py --network ../network/AT/AT_NN-1.onnx --falsifier nnenum --scaling AT --CPS_Rep MATLAB --initial ../validation/matlab/AT/initial_AT1.py'])

instance.append(['AT P2','runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/AT/property_66c.py --network ../network/AT/AT_NN-1.onnx --falsifier nnenum --scaling AT --CPS_Rep MATLAB --initial ../validation/matlab/AT/initial_AT6c.py'])

instance.append(['AFC P1','runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/AFC_P/property_11.py --network ../network/AFC_P/AFC_ES_5.onnx --falsifier nnenum --scaling AFC_P --CPS_Rep MATLAB --initial ../validation/matlab/AFC_P/initialAFC.py'])

instance.append(['CC P1','runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/Chasing_cars/property_111.py --network ../network/Chasing_cars/CC_ES_5.onnx --falsifier nnenum --scaling CC --CPS_Rep MATLAB --initial ../validation/matlab/CC/initial_CC.py'])

instance.append(['SC P1', 'runlim -s 6144 -r 3600 python3 NNFal.py --property ../property/SC/property_11.py --network ../network/SC/SC_ES_5.onnx --falsifier nnenum --scaling SC --CPS_Rep MATLAB --initial ../validation/matlab/SC/initial_SC.py'])




csv_fields = ['Models', 'Property name', '#Experiments', '#Refinements', 'Falsifying time', 'Validation time', 'Total time']
filename = "NNFal.csv"
csvFile = open(filename, 'w')
csvwriter = csv.writer(csvFile)
csvwriter.writerow(csv_fields)
#Number of tests/runs.
N=1 
for i in range(0, len(instance)):
	print("Running ",instance[i][0])
	total_fal_time = 0
	total_val_time = 0
	total_fv_time = 0
	successful_instance = 0
	for how_many in range(0, N):
		nnfal_output = str(subprocess.run(instance[i][1], capture_output=True, shell=True))
		#print(nnfal_output)
		status = nnfal_output.find("Found a trajectory!!!")
		if(status == -1):
			print("test:", how_many+1, " Can not find a safety violating trajectory.")
		else:
			print("test:", how_many+1, " Successful!!")
			successful_instance += 1
			refine = "Refinement needed:"
			ref = nnfal_output.find(refine)
			ref_start = int(ref)+len(refine)+0
			ref_end = int(ref)+len(refine)+8
			result2 = nnfal_output[ref_start:ref_end]
			ref_ = re.sub("[^\d\.]", "", result2)
			print("Refine: ", ref_)
			
			fal_str = "Falsifer time:"
			fal = nnfal_output.find(fal_str)
			fal_start = int(fal)+len(fal_str)+0
			fal_end = int(fal)+len(fal_str)+16
			result2 = nnfal_output[fal_start:fal_end];
			fal_time = re.sub("[^\d\.]", "", result2)
			print("falsification time: ", fal_time)
			
			val_str = "Validation time:"
			val = nnfal_output.find(val_str)
			val_start = int(val)+len(val_str)+0
			val_end = int(val)+len(val_str)+16
			result2 = nnfal_output[val_start:val_end];
			val_time = re.sub("[^\d\.]", "", result2)
			print("Validation time: ", val_time)
			
			tt = "Total time required:"
			tt_ = nnfal_output.find(tt)
			tt_start = int(tt_)+len(tt)+0
			tt_end = int(tt_)+len(tt)+16
			result2 = nnfal_output[tt_start:tt_end];
			tt_time = re.sub("[^\d\.]", "", result2)
			print("Total time: ", tt_time)
			total_fal_time += float(fal_time)
			total_val_time += float(val_time)
			total_fv_time += float(tt_time)
			csvwriter.writerow([instance[i][0].split()[0], instance[i][0].split()[1], how_many+1, ref_, fal_time, val_time, tt_time])
	if(successful_instance > 0):
		csvwriter.writerow([instance[i][0].split()[0], "Average Case", successful_instance, "", total_fal_time/successful_instance, total_val_time/successful_instance, total_fv_time/successful_instance])
	csvwriter.writerow(["", "", "", "", "", "", ""])


csvFile.close()


