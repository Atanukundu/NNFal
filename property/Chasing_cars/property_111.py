from dnnv.properties import *
import numpy as np

N = Network("N")

#x_min = np.array([0, 10, 20, 30, 40, 0])
#x_max = np.array([5, 15, 25, 35, 45, 100])

x_min = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
x_max = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])


y_max = 40


Forall(
	x, Implies(x_min <= x <= x_max, (N(x)[(0,4)]-N(x)[(0,3)]) <= y_max)
)



