from dnnv.properties import *
import numpy as np

N = Network("N")

x_min = np.array([[0, 0, 0]])
x_max = np.array([[1, 0, 1]])

#x_param: x1,x2,time


y_min_x1 = 0
y_max_x1 = 0.40
y_min_x2 = -0.500
y_max_x2 = -0.465

Forall(
	x, Implies(x_min <= x <= x_max, Or(Or(y_min_x1 > N(x)[(0,0)], N(x)[(0,0)] > y_max_x1), Or(y_min_x2 > N(x)[(0,1)], N(x)[(0,1)] > y_max_x2)))
)
