from dnnv.properties import *
import numpy as np

N = Network("N")


x_min = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.8571428571428571])
x_max = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])

y_min = 87.0
y_max = 87.30


Forall(
	x, Implies(x_min <= x <= x_max, Or(N(x)[(0,3)] < y_min, N(x)[(0,3)] > y_max))
)



