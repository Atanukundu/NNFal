from dnnv.properties import *
import numpy as np

N = Network("N")

x_min = np.array([0, 0, 0])
x_max = np.array([1, 1, 1])

#x_param: p,q,time

y_min_p = 0
y_max_p = 0.1
y_min_q = 0.13485
y_max_q = 0.15

Forall(
	x, Implies(x_min <= x <= x_max, Or(Or(y_min_p > N(x)[(0,0)], N(x)[(0,0)] > y_max_p), Or(y_min_q > N(x)[(0,1)], N(x)[(0,1)] > y_max_q)))
)
