from dnnv.properties import *
import numpy as np

N = Network("N")

x_min = np.array([ 0, 0, 0, 0, 0])
x_max = np.array([ 1, 1, 1, 1, 1])

#y_param: x1, x2, v1, v2, time


y_min_x1 =  22 
y_max_x1 =  23
y_min_x2 =  11
y_max_x2 =  12

Forall(
	x, Implies(x_min <= x <= x_max, Or(Or(y_min_x1 > N(x)[(0,0)], N(x)[(0,0)] > y_max_x1), Or(y_min_x2 > N(x)[(0,1)], N(x)[(0,1)] > y_max_x2)))
)



