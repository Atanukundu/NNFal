from dnnv.properties import *
import numpy as np

N = Network("N")

x_min = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
x_max = np.array([[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]])


#output_param: miu, mode

gamma = 0.007


Forall(
	x, Implies(x_min <= x <= x_max, N(x)[(0,0)] < gamma)
)





