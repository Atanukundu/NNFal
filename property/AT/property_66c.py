from dnnv.properties import *
import numpy as np

N = Network("N")

x_min = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
x_max = np.array([[0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0]])


#output_param: v, w, g

v = 65
w = 3000


Forall(
	x, Implies(x_min <= x <= x_max, Or(Not(N(x)[(0,1)] < w), N(x)[(0,0)] < v))
)



