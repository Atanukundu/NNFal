import matlab.engine
import os
import sys
import pandas as pd


arguments = sys.argv[1:]
eng = matlab.engine.start_matlab()



current_directory1 = os.getcwd()
os.chdir("../validation/matlab/SC")
current_directory = os.getcwd()
eng.addpath(current_directory)
#os.chdir(current_directory)


eng.workspace['S0'] = float(arguments[0])
eng.workspace['S1'] = float(arguments[1])
eng.workspace['S2'] = float(arguments[2])
eng.workspace['S3'] = float(arguments[3])
eng.workspace['S4'] = float(arguments[4])
eng.workspace['S5'] = float(arguments[5])
eng.workspace['S6'] = float(arguments[6])
eng.workspace['S7'] = float(arguments[7])
eng.workspace['S8'] = float(arguments[8])
eng.workspace['S9'] = float(arguments[9])
eng.workspace['S10'] = float(arguments[10])
eng.workspace['S11'] = float(arguments[11])
eng.workspace['S12'] = float(arguments[12])
eng.workspace['S13'] = float(arguments[13])
eng.workspace['S14'] = float(arguments[14])
eng.workspace['S15'] = float(arguments[15])
eng.workspace['S16'] = float(arguments[16])
eng.workspace['S17'] = float(arguments[17])
eng.workspace['S18'] = float(arguments[18])
eng.workspace['S19'] = float(arguments[19])
eng.workspace['S20'] = float(arguments[20])

fun_name = "run_SC"
eng.feval(fun_name, nargout=0)

os.chdir(current_directory1)
#print(output_matlab)
df1 = pd.read_csv('yout.csv', names=["A", "B", "C", "P"])
#print(df1)

count1 = len(df1[df1['P'] < 87])
count2 = len(df1[df1['P'] > 87.30])
#print(count1)
#print(count2)
if(count1 or count2):
	print("Validated")
else: 
	print("NOT FOUND")



eng.quit()
