import matlab.engine
import os
import sys
eng = matlab.engine.start_matlab()

current_directory = os.getcwd()
print(current_directory)
#folder_path = ""
os.chdir("../validation/matlab/F_16")
current_directory = os.getcwd()
eng.addpath(current_directory)
folder_path2 = current_directory + "/src/main/Simulink"
eng.addpath(folder_path2)
folder_path3 = current_directory + "/src/main/utils"
eng.addpath(folder_path3)
folder_path4 = current_directory + "/src/main/Autopilot"
eng.addpath(folder_path4)
folder_path5 = current_directory + "/src/main/F16_Model"
eng.addpath(folder_path5)
folder_path6 = current_directory + "/src/main/FlightControllers/controlGains"
eng.addpath(folder_path6)
folder_path7 = current_directory + "/src/main/FlightControllers"
eng.addpath(folder_path7)
folder_path8 = current_directory + "/src/main/F16_Model/NonlinearModel"
eng.addpath(folder_path8)


eng.workspace['phig'] = float(sys.argv[1])
eng.workspace['thetag'] = float(sys.argv[2])
eng.workspace['psig'] = float(sys.argv[3])
tf = eng.SimConfig(nargout=0)

eng.quit()
