disp('Graphing Output');

if size(t_out, 1) > 0
    figure(1);
    clf
    %subplot(3,1,1)
    hold on;
    grid on;
    title('Altitude History');
    xlabel('Time (sec)');
    ylabel('Altitude (ft)');
    hold on;
    plot(t_out, x_f16_out(:,12), 'b--');     % alt
    legend('Alt (ft)', ...
        'location','NorthEast')
    
else
    disp('No Data from simulation run?');
end