
t__ = [0; 5; 10; 15; 20; 25; 30];
u__ = [th0 br0; th0 br0; th1 br1; th1 br1; th2 br2; th2 br2; th3 br3];


u = [t__, u__];
T = 30;

disp('before simulation');
result = sim('Autotrans_shift', ...
  'StopTime', 'T', ...
  'LoadExternalInput', 'on', 'ExternalInput', 'u', ...
  'SaveTime', 'on', 'TimeSaveName', 'tout', ...
  'SaveOutput', 'on', 'OutputSaveName', 'yout', ...
  'SaveFormat', 'Array');
tout = result.tout;
yout = result.yout;
dlmwrite('yout.csv',yout,'delimiter',',');
disp('success');

