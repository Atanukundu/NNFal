import matlab.engine
import os
import sys


eng = matlab.engine.start_matlab()


os.chdir("../validation/matlab/AT")
current_directory = os.getcwd()
eng.addpath(current_directory)
os.chdir(current_directory)

eng.workspace['th0'] = float(sys.argv[1])
eng.workspace['br0'] = float(sys.argv[2])
eng.workspace['th1'] = float(sys.argv[3])
eng.workspace['br1'] = float(sys.argv[4])
eng.workspace['th2'] = float(sys.argv[5])
eng.workspace['br2'] = float(sys.argv[6])
eng.workspace['th3'] = float(sys.argv[7])
eng.workspace['br3'] = float(sys.argv[8])

fun_name = "run_AT"
eng.feval(fun_name, nargout=0)

eng.quit()
