import matlab.engine
import os
import sys

y1 = float(sys.argv[1])
y2 = float(sys.argv[2])
y3 = float(sys.argv[3])
y4 = float(sys.argv[4])
y5 = float(sys.argv[5])

eng = matlab.engine.start_matlab()

current_directory = os.getcwd()
#print(current_directory)
os.chdir("../validation/matlab/CC")
current_directory = os.getcwd()
#print(current_directory)
folder_path2 = current_directory + "/chasing-cars"
eng.addpath(folder_path2)
os.chdir(current_directory+"/chasing-cars")
#print(eng.path())
# print(os.getcwd())

#phig = 0.890010;
#thetag = -1.256600;
#psig = -0.8206607262023999;
eng.workspace['y1'] = y1
eng.workspace['y2'] = y2
eng.workspace['y3'] = y3
eng.workspace['y4'] = y4
eng.workspace['y5'] = y5
tf = eng.test_cars(nargout=0)
# tf2 = eng.sim('AeroBenchSim_2017b')
#tf2 = eng.data_generator(nargout=0)

eng.quit()
