import matlab.engine
import os
import sys
import pandas as pd


arguments = sys.argv[1:]
eng = matlab.engine.start_matlab()



current_directory1 = os.getcwd()
os.chdir("../validation/matlab/CC")
current_directory = os.getcwd()
folder_path2 = current_directory + "/chasing-cars"
eng.addpath(folder_path2)
os.chdir(folder_path2)



eng.workspace['th0'] = float(arguments[0])
eng.workspace['br0'] = float(arguments[1])
eng.workspace['th1'] = float(arguments[2])
eng.workspace['br1'] = float(arguments[3])
eng.workspace['th2'] = float(arguments[4])
eng.workspace['br2'] = float(arguments[5])
eng.workspace['th3'] = float(arguments[6])
eng.workspace['br3'] = float(arguments[7])
eng.workspace['th4'] = float(arguments[8])
eng.workspace['br4'] = float(arguments[9])
eng.workspace['th5'] = float(arguments[10])
eng.workspace['br5'] = float(arguments[11])


fun_name = 'run_CC_inputs'
eng.feval(fun_name, nargout=0)

os.chdir(current_directory1)

df2=pd.DataFrame()
df1 = pd.read_csv('yout.csv', names=["Car1", "Car2", "Car3","Car4","Car5"])
df2['y5-y4']=df1['Car5'].sub(df1['Car4'],axis=0)

if(len(df2[df2['y5-y4'] > 40])):
	print("Validated")
else:
	print("NOT FOUND")


eng.quit()
