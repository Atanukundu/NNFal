t__ = [  0;  20;  40;  60;  80; 100];
u__ = [0 1; 1 0; 0 1; 1 0; 0 1; 1 0];
%5.6629913449287415, 18.08247745037079, 21.28278858959675, 34.80372369289398, 43.90335011482239, 44.734808802604675
u = [t__, u__];
T = 100;

[tout, yout] = run_cars(u, T);

%dlmwrite('check_u.csv',u,'delimiter',',');
%dlmwrite('check_u__.csv',u__,'delimiter',',');
dlmwrite('check_yout.csv',yout,'delimiter',',');
%dlmwrite('check_tout.csv',tout,'-append','delimiter',',');
