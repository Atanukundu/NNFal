for i=1:1
    disp("Iteration: "+i);
    r = randi([0 1],1,6);
    
    %This loop tries to remove bias in the random generator
    for j = 2:2:length(r)
        if r(j) == r(j-1)
            r(j) = ~r(j);
        end
    end
    u__= r';
    disp(u__);
    %Throttle value is set above this sets the break value
    for j=1:6
        u__(j,2)=(1-u__(j,1));
    end

    test_cars
    dlmwrite('u(0.10.20.30.40).csv',u,'-append','delimiter',',');
    dlmwrite('u__(0.10.20.30.40).csv',u__,'-append','delimiter',',');
    dlmwrite('yout(0.10.20.30.40).csv',yout,'-append','delimiter',',');
    dlmwrite('tout(0.10.20.30.40).csv',tout,'-append','delimiter',',');
end