
t__ = [0; 5; 10; 15; 20; 25; 30; 35; 40; 45; 50];
u__ = [th0 om0; th0 om0; th1 om1; th1 om1; th2 om2; th2 om2; th3 om3; th3 om3; th4 om4; th4 om4; th5 om5];

u = [t__, u__];
T = 50;


assignin('base','U',u);
assignin('base','StopTime',T);

disp('before simulation');
result = sim('AbstractFuelControl_M1', ...
  'StopTime', 'StopTime', ...
  'LoadExternalInput', 'on', 'ExternalInput', 'U', ...
  'SaveTime', 'on', 'TimeSaveName', 'tout', ...
  'SaveOutput', 'on', 'OutputSaveName', 'yout', ...
  'SaveFormat', 'Array');
tout = result.tout;
yout = result.yout;
index = find(tout(:,1) == 11);
writematrix(yout(index:end,:),'yout.csv');
disp('success');

