import matlab.engine
import os
import sys


eng = matlab.engine.start_matlab()


os.chdir("../validation/matlab/AFC_P")
current_directory = os.getcwd()
eng.addpath(current_directory)
os.chdir(current_directory)

eng.workspace['th0'] = float(sys.argv[1])
eng.workspace['om0'] = float(sys.argv[2])
eng.workspace['th1'] = float(sys.argv[3])
eng.workspace['om1'] = float(sys.argv[4])
eng.workspace['th2'] = float(sys.argv[5])
eng.workspace['om2'] = float(sys.argv[6])
eng.workspace['th3'] = float(sys.argv[7])
eng.workspace['om3'] = float(sys.argv[8])
eng.workspace['th4'] = float(sys.argv[9])
eng.workspace['om4'] = float(sys.argv[10])
eng.workspace['th5'] = float(sys.argv[11])
eng.workspace['om5'] = float(sys.argv[12])

eng.workspace['simTime'] = float(50)
eng.workspace['measureTime'] = float(1)
eng.workspace['fault_time'] = float(60)
eng.workspace['spec_num'] = float(1)
eng.workspace['fuel_inj_tol'] = float(1)
eng.workspace['MAF_sensor_tol'] = float(1)
eng.workspace['AF_sensor_tol'] = float(1)

fun_name = "run_AFC"
eng.feval(fun_name, nargout=0)

eng.quit()
