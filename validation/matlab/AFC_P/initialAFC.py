import matlab.engine
import os
import sys
import pandas as pd


arguments = sys.argv[1:]
eng = matlab.engine.start_matlab()

current_directory1 = os.getcwd()
os.chdir("../validation/matlab/AFC_P")
current_directory = os.getcwd()
eng.addpath("../validation/matlab/AFC_P")
#os.chdir(current_directory)

eng.workspace['th0'] = float(arguments[0])
eng.workspace['om0'] = float(arguments[1])
eng.workspace['th1'] = float(arguments[2])
eng.workspace['om1'] = float(arguments[3])
eng.workspace['th2'] = float(arguments[4])
eng.workspace['om2'] = float(arguments[5])
eng.workspace['th3'] = float(arguments[6])
eng.workspace['om3'] = float(arguments[7])
eng.workspace['th4'] = float(arguments[8])
eng.workspace['om4'] = float(arguments[9])
eng.workspace['th5'] = float(arguments[10])
eng.workspace['om5'] = float(arguments[11])

eng.workspace['simTime'] = float(50)
eng.workspace['measureTime'] = float(1)
eng.workspace['fault_time'] = float(60)
eng.workspace['spec_num'] = float(1)
eng.workspace['fuel_inj_tol'] = float(1)
eng.workspace['MAF_sensor_tol'] = float(1)
eng.workspace['AF_sensor_tol'] = float(1)

fun_name = "run_AFC"
eng.feval(fun_name, nargout=0)

os.chdir(current_directory1)

df1 = pd.read_csv('yout.csv', names=["miu", "Mode"])

if(len(df1[df1['miu'] > 0.007])):
	print("Validated")
else:
	print("NOT FOUND")


eng.quit()
