# Install DNNF package for CE generation
	pip3 install dnnf
	
# Install DNNV package for CE generation
	pip3 install dnnv
	
# Install NNENUM in DNNV framework.

	dnnv_manage install nnenum

# Install Xspeed for CE verification
	cd validation/
	git clone https://gitlab.com/Atanukundu/XSpeed-plan
	
	# Install prerequisites for Xspeed
	cd XSpeed-plan/
	mkdir installer
	cp install.sh installer/
	cd installer
	chmod +x install.sh
	./install.sh
	cd ..
	sudo rm -r installer

	# Build XSpeed-plan
	cd build/
	sudo make all
	
# Install matlabengine=9.13.7
	pip3 install matlabengine==9.13.7
